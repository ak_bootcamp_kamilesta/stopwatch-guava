package ak.bootcamp;


import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) {

        StopwatchClass stopwatch = new StopwatchClass();
        TimeUnit timeUnit = TimeUnit.NANOSECONDS;
        int numberToMultiply = 2;

        stopwatch.startCounting();
        Math.pow(numberToMultiply, 2);
        stopwatch.stopCounting();
        long elapsedPow = stopwatch.elapsedTime(timeUnit);

        stopwatch.resetClock();
        stopwatch.startCounting();
        int multiplyResult = numberToMultiply * numberToMultiply;
        long elapsedMultiplied = stopwatch.elapsedTime(timeUnit);

        printResult(elapsedPow, elapsedMultiplied, timeUnit);
    }

    public static void printResult(long elapsedPow, long elapsedMultiply, TimeUnit timeUnit) {
        System.out.println("The result is:");
        System.out.println("Time elapsed for power function: " + elapsedPow + timeSuffix(timeUnit));
        System.out.println("Time elapsed for multiplication: " + elapsedMultiply + timeSuffix(timeUnit));
        System.out.println("The difference is: " + (elapsedPow - elapsedMultiply) + timeSuffix(timeUnit));
    }

    public static String timeSuffix(TimeUnit timeUnit) {
        switch (timeUnit) {
            case NANOSECONDS:
                return " ns";
            case MILLISECONDS:
                return " ms";
            case MICROSECONDS:
                char micro = (char) 181;
                return " " + micro + "s";
            default:
                return "";
        }
    }
}
