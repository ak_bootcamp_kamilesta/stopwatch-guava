package ak.bootcamp;

import com.google.common.base.Stopwatch;
import java.util.concurrent.TimeUnit;

public class StopwatchClass {

    private final Stopwatch stopwatch;

    public StopwatchClass() {
        this.stopwatch = Stopwatch.createUnstarted();
    }

    public void startCounting(){
        if (stopwatch.isRunning()){
            System.out.println("Stopwatch is already running!");
        } else {
            stopwatch.start();
        }
    }

    public void stopCounting(){
        if (!stopwatch.isRunning()){
            stopwatch.stop();
        }
    }

    public long elapsedTime(TimeUnit timeUnit){
        return stopwatch.elapsed(timeUnit);
    }

    public void resetClock(){
        stopwatch.reset();
    }
}
